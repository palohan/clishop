### Introduction ###
Initially, this was a term assignment for a college friend. As more students started asking for it, I kept adding new features while polishing my C++ skills, with emphasis on the latest C++. I like to train my brain with it and still add to it occasionally.

You are free to use the project in whatever way, just please do not make make money from it.

### Summary ###
A console app, written in standard C++14. Builds fine with g++ and VC++.

It represents a simple shop with a hierarchy of entities: shop -> orders -> items -> parts. Entities can have properties and can be added, deleted, edited, printed to stdout / file. 

Supports slovak (limited) and english languages. 

Shop accepts commands either in a long or a short form: edit order 1 == e o 1

A "walktrough" can be found at **docs/description-EN.txt**

**help** command shows the allowed commands for at the current prompt.


### How do I get set up? ###

Just **cmake** - EASY! Visual studio 2017 comes with built in cmake support; otherwise, on unix systems you would do:

```
#!

cd clishop
mkdir build
cd build
cmake ..
make
```

### Coming soon ###
- ambigous entities - cannot distinguish between them if they have the same short form. This would need some major rework.
- better language support

# Have fun! #