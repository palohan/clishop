#include "TextKey.h"
#include "SymbolInfo.h"
#include "misc.h"


TextKeyMap TextKeyMap_EN =
{
	{ TextKey::badCommand,		"Unknown command"},
	{ TextKey::ambigCommand,	"Ambiguous command" },

	{ TextKey::badEntity,	    "Unknown entity" },
	{ TextKey::ambigEntity,		"Ambiguous entity" },
	{ TextKey::noEntity,        "Expecting entity" },
	{ TextKey::wrongEntity,	    "Wrong entity" },
	
	{ TextKey::badProp,		    "Unknown property" },
	{ TextKey::ambigProp,		"Ambiguous property" },
	{ TextKey::noProp,          "Expecting property" },

	{ TextKey::badPropVal,		"Wrong property type" },
	{ TextKey::noPropVal,		"Expecting property value" },
	
	{ TextKey::badID,		    "Entity ID is not a number" },
	{ TextKey::badIDorAll,		"Expecting entity ID or *" },
	{ TextKey::noID,			"Expecting entity ID" },
	{ TextKey::noIDorAll,		"Expecting entity ID or *" },

	{ TextKey::badExtra,	    "Extra unneeded input" },
	{ TextKey::badInput,		"Bad input" },
	{ TextKey::badPlural,		"Plural entity not allowed" },
	
	{ TextKey::error,			"Error: " },

	{ TextKey::all			,	"*" },

	{ TextKey::add			,	"add"},
	{ TextKey::edit		  	,	"edit"},
	{ TextKey::del		  	,	"delete"},
	{ TextKey::set		  	,	"set"},
	{ TextKey::reset	  	,	"reset"},
	{ TextKey::list		  	,	"list"},
	{ TextKey::print	  	,	"print"},
	{ TextKey::quit		  	,	"quit"},
	{ TextKey::help		  	,	"help" },

	{ TextKey::shop		  	,	"shop"},
	{ TextKey::order	  	,	"order"},
	{ TextKey::item		  	,	"item"},
	{ TextKey::part		  	,	"part"},

	{ TextKey::price	  	,	"price"},
	{ TextKey::year		  	,	"year"},
	{ TextKey::type		  	,	"type"},
	{ TextKey::name		  	,	"name"},
	{ TextKey::description	,	"description"},

	{ TextKey::language		,	"language" },
	{ TextKey::noLanguage	,	"Expecting language" },
	{ TextKey::badLanguage	,	"Wrong language" },
	{ TextKey::ambigLanguage,	"Ambiguous language" },

	{ TextKey::slovak		,	"slovak" },
	{ TextKey::english		,	"english" },
};


TextKeyMap TextKeyMap_SK =
{
	{ TextKey::badCommand,		"Neznamy prikaz" },
	{ TextKey::badEntity,	    "Neznama entita" },
	{ TextKey::noEntity,        "Chyba entita" },
	{ TextKey::wrongEntity,	    "Nespravna entita" },

	{ TextKey::badProp,		    "Neznama vlastnost" },
	{ TextKey::noProp,          "Chyba vlastnost" },

	{ TextKey::badPropVal,		"Nespravny typ vlastnosti" },
	{ TextKey::noPropVal,		"Chyba hodnota vlastnosti" },

	{ TextKey::badID,		    "ID entity nie je cislo" },
	{ TextKey::noID,			"Chyba cislo entity" },
	{ TextKey::noIDorAll,		"Chyba cislo entity alebo *" },

	{ TextKey::badExtra,	    "Prebytocny input" },
	{ TextKey::badInput,		"Zly input" },

	{ TextKey::error,			"Chyba: " },

	{ TextKey::all			,	"*" },

	{ TextKey::add			,	"pridaj" },
	{ TextKey::edit		  	,	"uprav" },
	{ TextKey::del		  	,	"vymaz" },
	{ TextKey::set		  	,	"nastav" },
	{ TextKey::reset	  	,	"reset" },
	{ TextKey::list		  	,	"vypis" },
	{ TextKey::print	  	,	"tlac" },
	{ TextKey::quit		  	,	"ukonci" },
	{ TextKey::help		  	,	"pomoc" },

	{ TextKey::shop		  	,	"obchod" },
	{ TextKey::order	  	,	"objednavka" },
	{ TextKey::item		  	,	"polozka" },
	{ TextKey::part		  	,	"cast" },

	{ TextKey::price	  	,	"cena" },
	{ TextKey::year		  	,	"rok" },
	{ TextKey::type		  	,	"typ" },
	{ TextKey::name		  	,	"meno" },
	{ TextKey::description	,	"popis" },

	{ TextKey::language	,	"jazyk" },
	{ TextKey::noLanguage	,"Chyba jazyk" },
	{ TextKey::badLanguage	,"Nespravny jazyk" },

	{ TextKey::slovak	,	"slovensky" },
	{ TextKey::english	,	"anglicky" },
};


TextKeyMap* TextKeyMap_CUR = &TextKeyMap_EN;

const char* getText(TextKey _this)
{
	auto it = TextKeyMap_CUR->find(_this);
	
	if (it == TextKeyMap_CUR->end())
		it = TextKeyMap_EN.find(_this);

	return it->second;
}


std::ostream & operator<<(std::ostream & out, const TextKey & _this)
{
	return out << getText(_this);
}


CTextKey::CTextKey(TextKey key) :
	_key(key)
{}

CTextKey::operator const char* () const
{
	return getText(_key);
}

bool CTextKey::startsWith(const std::string& strSmall) const
{
	return misc::startsWith(*this, strSmall.data());
}