using namespace std;

#include <iostream>
#include <exception>
#include "Entity.h"
#include "shop.h"

//tu zacina program
int main()
{
	int retval = 0;
    //ak sa v TRY bloku vyhodi vynimka, skoci sa do niektoreho z CATCH blokov. vynimka sa vyhadzuje slovom THROW
	try
	{
		ShopEntity shop(Meta::Entity::shop);

		ShopRunner container(&shop);

		//bude bezat dovtedy kym run() nevrati false
		while (container.run());
	}
	//catch bloky vypisu text vynimky
	catch (exception& ex)
	{
	    cerr << ex.what() << endl;
		retval = -1;
	}
	catch (const char* c)
	{
		cerr << c << endl;
		retval = -1;
	}
	//toto zachyti uplne vsetko
	catch (...)
	{
		cerr << "caught exception" << endl;
		retval = -1;
	}

	if (retval)
		getchar();

	return retval;
}
