#pragma once

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <array>
#include <stdexcept>

#include <string.h>

#include "misc.h"
#include "parsing.h"
#include <map>
#include <memory>

class EntityBase
{
public:
	const EntityInfo _entityInfo;
	EntityBase * const _parent;

	std::string _prompt, _allPrompt;

	EntityBase(Meta::Entity entity, EntityBase* parent = nullptr);

	EntityBase* getParent() const;

    //zakladna trieda BY MALA MAT virtualny destruktor
	virtual ~EntityBase();
};


//Entity DEDI od 2 tried
class Entity : public EntityBase, public IOutputable
{
	protected:
	size_t _id, _lastChildId;

    //"slovnik" - ID identifikuju detske entity
	std::map<size_t, std::unique_ptr<Entity>> _childEntities;


	public:
	Entity(Meta::Entity entity, size_t id = 1, Entity* parent = nullptr);
	virtual ~Entity() = default;

    //metoda s const modifikatorom - konstantnost objektu ostane neporusena
	bool canHaveChildren() const;

	bool canHaveChildOf(Meta::Entity otherEntity) const;

	bool isParentOf(Meta::Entity otherEntity, size_t id) const;

	bool hasChild(size_t id) const;

	Entity* add();


	//Entity* edit(size_t id);

	Entity* getChild(size_t id) const;

	void del(size_t id);

	void delAll();

	std::ostream& print(std::ostream& out) const override;


	//vypis do suboru
	bool print() const;

	void printPrompt() const;

	virtual double getTotalPrice() const;												//placeholder

protected:
	template<class This>
	static std::unique_ptr<Entity> addInternal(size_t newId, This& aThis);

	virtual std::unique_ptr<Entity> addInternal(size_t newId);
	virtual void listInternal(std::ostream& out, const std::string& tabs) const;					//placeholder for derived classes;

	private:
	void listImpl(std::ostream& out, size_t tabLevel = 0) const;

	auto getChildIter(size_t id) const -> decltype(_childEntities)::const_iterator;
};

template<class This>
std::unique_ptr<Entity> Entity::addInternal(size_t newId, This& aThis)
{
	auto childEntity = aThis._entityInfo.getNextSymbol();

	return std::unique_ptr<Entity>(new This(childEntity, newId, &aThis));
}

class ShopEntity : public Entity
{
	std::map<Meta::Prop, misc::MyVariant> _propMap;

//MAKRA s parametrami
#define propVal(prop) _propMap.at(Meta::Prop::prop)
#define propToStr(attr) #attr << ": " << propVal(attr) << std::endl

public:
    ShopEntity(Meta::Entity entity, size_t id = 1, Entity* parent = nullptr);

	bool setProp(Meta::Prop prop, const misc::MyVariant& vrt);

	void resetProp(Meta::Prop symbol);

    //TVORIVA REKURZIBNA METODA
	double getTotalPrice() const;

    std::unique_ptr<Entity> addInternal(size_t newId) override;

	virtual void listInternal(std::ostream& out, const std::string& tabs) const override;					//placeholder for derived classes;

	std::ostream& printHelp(std::ostream & out) const;
	std::ostream& printHelpOne(const CommandInfo& cInfo, std::ostream& out) const;
};