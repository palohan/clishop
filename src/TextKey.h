#pragma once

#include <iostream>
#include <map>


#define TextKeyPair(_enum, name)		{ _enum::name, TextKey::name }



enum class TextKey
{
	badCommand,
	ambigCommand,

	badEntity,
	ambigEntity,

	badProp,
	ambigProp,

	noEntity,
	noProp,
	wrongEntity,
	badID,
	badIDorAll,
	noID,
	noIDorAll,

	badPropVal,
	noPropVal,

	badExtra,
	badInput,
	badPlural,

	error

	,all
	
	,add, edit, del, set, reset, list, print, quit, help,

	shop, order, item, part, 

	price, year, type, name, description, 

	language,
	badLanguage,
	ambigLanguage,
	noLanguage,

	slovak, english
};


const char* getText(TextKey);
std::ostream& operator<< (std::ostream& out, const TextKey& _this);

class CTextKey
{
	public:
	const TextKey _key;

	CTextKey(TextKey key);
	operator const char* () const;

	bool startsWith(const std::string& strSmall) const;
};

typedef const std::map<TextKey, const char* const> TextKeyMap;

extern TextKeyMap* TextKeyMap_CUR;
extern TextKeyMap TextKeyMap_SK;
extern TextKeyMap TextKeyMap_EN;
