#include <iostream>
#include <sstream>
#include <algorithm>
#include <cstring>

#include "parsing.h"

using namespace std;

#define ParseResultTextKey(name)	TextKeyPair(Result, name)

// definicia chybovych hlasok validacie. kazdy KLUC definuje jeden string
Parser::ParseResultTextKeys Parser::_errorStrs =
{
	ParseResultTextKey(badCommand),
	ParseResultTextKey(ambigCommand),

	ParseResultTextKey(badEntity),
	ParseResultTextKey(ambigEntity),
	ParseResultTextKey(noEntity),
	ParseResultTextKey(wrongEntity),

	ParseResultTextKey(badProp),
	ParseResultTextKey(ambigProp),
	ParseResultTextKey(noProp),
	ParseResultTextKey(badPropVal),
	ParseResultTextKey(noPropVal),

	ParseResultTextKey(badID),
	ParseResultTextKey(badIDorAll),
	ParseResultTextKey(noID),
	ParseResultTextKey(noIDorAll),
	
	ParseResultTextKey(badLanguage),
	ParseResultTextKey(ambigLanguage),
	ParseResultTextKey(noLanguage),
			
	ParseResultTextKey(badExtra),
	ParseResultTextKey(badInput),

	ParseResultTextKey(badPlural),
};

Meta::Command ParserState::getCommand() const
{
	return _commandInfo.getSymbol();
}

Meta::Entity ParserState::getEntity() const
{
	return _entityInfo.getSymbol();
}

size_t ParserState::getId() const
{
	return _id;
}

void ParserState::reset()
{
	_commandInfo = Meta::Command::last;
	_entityInfo = Meta::Entity::last;
	//_prop = Meta::Prop::last;
	_allIds = false;
	_id = 0;
	
	_vecAbmigous.clear();
}


ostream& ParserState::print(ostream& out) const
{
	out << printNamedVal(command, _commandInfo.getName());
	out << printNamedVal(entity, _entityInfo.getName());
    out << attrToStr(allIds);
    out << attrToStr(id);

	return out;
}


TextKey Parser::errorMsg(Result result)
{
	return _errorStrs.find(result)->second;
}

Parser::Result Parser::getInput(ParserState& comObject)
{
	Result parseResult = validateImpl(comObject);
	comObject._isValid = parseResult > Result::badInput;

	if (comObject._isValid)
	{
		;//clog << "good input" << endl;
	}
	else
	{
		if (_prevPos != -1)
        {
			streamoff pos = _prevPos;
			
            while (isspace(inputLine[pos]))							//move to 1st visible char
                ++pos;

			if (inputLine.length() == (size_t)pos)
				pos = _prevPos + 1;

			cerr << inputLine << endl;										//vypis cely riadok
            cerr << string(pos, ' ') << '^' << endl;					//daj marker na zle slovo
        }

		if (!comObject._vecAbmigous.empty())
			parseResult = makeAmbig(parseResult);

		cerr << TextKey::error << errorMsg(parseResult) << endl;

		if (!comObject._vecAbmigous.empty())
		{
			cerr << "Possible candidates:";

			for (const auto& ambig : comObject._vecAbmigous)
				cerr << '\t' << ambig;

			cerr << endl;
		}
	}

	return parseResult;
}

Parser::Result Parser::validateImpl(ParserState& comObject)
{
	_prevPos = 0;
	string sTarget;

	getline(cin, inputLine);

	if (inputLine.empty())
		return Parser::Result::none;							//dont act on it

	iss.clear();												//resets the stream - ABSOLUTELY NECCESSARY
	iss.str(inputLine);											//puts inputLine into iss

	iss >> skipws >> sTarget;								//only need to set skipws once
	
	comObject.reset();

	if (iss.fail())
		return Parser::Result::badCommand;
		
	//1ST, EXTRACT COMMAND
	if (!comObject.findKeywordsICase(comObject._commandInfo, sTarget))
		return Parser::Result::badCommand;

	//clog << "index1 " << commandIndex << endl;

	
	if (comObject._commandInfo.hasProp(CommandInfo::doesChildren))						//operate on child entity
	{
		_prevPos = iss.tellg();
		iss >> sTarget;											//read entity

		if (iss.fail())																//stream error => command syntax error
			return Parser::Result::noEntity;
		
		//EXTRACT ENTITY
		if (!comObject.findKeywordsICase(comObject._entityInfo, sTarget))
			return Parser::Result::badEntity;

		//clog << "index2 " << entityIndex << endl;


		if (comObject._entityInfo.hasProp(EntityInfo::isSealed))
			return Parser::Result::wrongEntity;														//shop is top level; cant call it directly


		if (comObject._commandInfo.hasProp(CommandInfo::doesID))
		{
			if (!comObject._commandInfo.hasProp(CommandInfo::doesPlural) || !comObject._allIds)			//doesnt do plural => needs ID OR does plural but has not ID. KEEP ORDER OF OPERANDS!!!
			{
				_prevPos = iss.tellg();
				iss >> comObject._id;

				if (iss.fail())
				{
					if (iss.eof())									//no input found
					{
						if (comObject._commandInfo.hasProp(CommandInfo::doesPlural))			//short entity name specified and command does plural => assume all IDs
							return Parser::Result::noIDorAll;
						else
							return Parser::Result::noID;
					}
					else											//input == NaN
					{
						if (comObject._commandInfo.hasProp(CommandInfo::doesPlural))
						{
							iss.clear();
							iss >> sTarget;
							comObject._allIds = misc::startsWith(getText(TextKey::all), sTarget.data());

							if (!comObject._allIds)
								return Parser::Result::badIDorAll;
						}
						else
							return Parser::Result::badID;
					}
				}
			}
		}
	}
	else if (comObject._commandInfo.hasProp(CommandInfo::doesProps))			//operate on property
	{
		_prevPos = iss.tellg();
		iss >> sTarget;											//read prop

		if (iss.fail())																//stream error => command syntax errror
			return Parser::Result::noProp;

		if (!comObject.findKeywordsICase(comObject._propertyInfo, sTarget))
			return Parser::Result::badProp;

		//clog << "index2 " << propIndex << endl;
		

		if (comObject._commandInfo.hasProp(CommandInfo::doesPropVal))
		{
			_prevPos = iss.tellg();

			if (!comObject._variant.setGeneric(iss, comObject._propertyInfo.getType()))
			{
				if (iss.eof())
					return Parser::Result::noPropVal;
				else
					return Parser::Result::badPropVal;						//wrong prop type
			}
		}
	}
	else if (comObject._commandInfo == Meta::Command::language)
	{
		_prevPos = iss.tellg();
		iss >> sTarget;											//read prop

		if (iss.fail())																//stream error => command syntax errror
			return Parser::Result::noLanguage;
		
		if (!comObject.findKeywordsICase(comObject._langInfo, sTarget))
			return Parser::Result::badLanguage;

		//clog << "index2 " << propIndex << endl;
	}


	ws(iss);																//consume trailing spaces
	_prevPos = iss.tellg();

	if (!iss.eof())
		return Parser::Result::badExtra;						//extra unneeded input

	return Parser::Result::good;
}
