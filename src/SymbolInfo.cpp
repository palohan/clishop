#include <algorithm>
#include <functional>

#include "SymbolInfo.h"

using namespace std;


#ifdef _WIN32
#define TEMPL_SPEC					//static member template specialization inicializacia sa nedeje
#else
#define TEMPL_SPEC template<>
#endif // _WIN32


// definicie statickych clenov . su const, lebo sa po deklaracii nebudu menit. staticke atributy treba definovat(dat im hodnotu) v CPP subore
PropertyInfo::PropTypeMap PropertyInfo::propTypes
{ 
	{ Enum::price		,	misc::Type::floating },
	{ Enum::year		,	misc::Type::uint	 },
	{ Enum::type		,	misc::Type::string	 },
	{ Enum::name		,	misc::Type::string	 },
	{ Enum::description	,	misc::Type::string	 },
};


#define EnumTextKey(name)	TextKeyPair(Enum, name)

TEMPL_SPEC
CommandInfo::TextKeyMap SymbolInfo<Meta::Command>::symbolNames
{
	EnumTextKey(add),
	EnumTextKey(edit),
	EnumTextKey(del),
	EnumTextKey(set),
	EnumTextKey(reset),
	EnumTextKey(list),
	EnumTextKey(print),
	EnumTextKey(quit),
	EnumTextKey(help),
	EnumTextKey(language),
};

TEMPL_SPEC
EntityInfo::TextKeyMap SymbolInfo<Meta::Entity>::symbolNames
{
	EnumTextKey(shop),
	EnumTextKey(order),
	EnumTextKey(item),
	EnumTextKey(part),
};

TEMPL_SPEC
PropertyInfo::TextKeyMap SymbolInfo<Meta::Prop>::symbolNames
{
	EnumTextKey(price),
	EnumTextKey(year),
	EnumTextKey(type),
	EnumTextKey(name),
	EnumTextKey(description),
};

TEMPL_SPEC
LangInfo::TextKeyMap LangInfo::symbolNames
{
	EnumTextKey(english),
	EnumTextKey(slovak),
};


TEMPL_SPEC
const CommandInfo::PropMap SymbolInfo_Prop<Meta::Command>::propMap =
{
	{ Meta::Command::add,	CommandInfo::doesChildren },
	{ Meta::Command::edit,	CommandInfo::doesChildren | CommandInfo::doesID },
	{ Meta::Command::del,	CommandInfo::doesChildren | CommandInfo::doesID | CommandInfo::doesPlural },
	{ Meta::Command::set,	CommandInfo::doesProps | CommandInfo::doesPropVal },
	{ Meta::Command::reset,	CommandInfo::doesProps },
	{ Meta::Command::list,	0 },
	{ Meta::Command::print,	0 },
	{ Meta::Command::quit,	0 },
};

TEMPL_SPEC
const EntityInfo::PropMap SymbolInfo_Prop<Meta::Entity>::propMap =
{
	{ Meta::Entity::shop, 	EntityInfo::doesChildren | EntityInfo::isSealed },
	{ Meta::Entity::order,	EntityInfo::doesChildren | EntityInfo::doesProps },
	{ Meta::Entity::item, 	EntityInfo::doesChildren | EntityInfo::doesProps },
	{ Meta::Entity::part, 	EntityInfo::doesProps },
};

stringstream CommandInfo::usage() const
{
	stringstream out;

	if (hasProp(doesChildren))
	{
		if ((getProps() ^ doesChildren) == 0)				//no IDs or plural
			out << _name << " <entity>" << endl;
		else
		{
			if (hasProp(CommandInfo::doesID))
				out << _name << " <entity> <ID>" << endl;

			if (hasProp(CommandInfo::doesPlural))
				out << _name << " <entity> " << getText(TextKey::all) << endl;
		}
	}
	else if (hasProp(doesProps))
	{
		for (const auto& [theSymbol, theType] : PropertyInfo::propTypes)
		{
			out << _name << " " << PropertyInfo::getSymbolName(theSymbol);

			if (hasProp(CommandInfo::doesPropVal))
			{
				out << getTypeHint(theType);
			}

			out << endl;
		}
	}
	else
		out << _name << endl;

	return out;
}
