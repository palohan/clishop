#pragma once

#include "parsing.h"
#include "Entity.h"

class ShopRunner
{
	Parser _parser;
	ShopEntity* _curEntity;
	ParserState _parserState;

	bool isValidInput() const;
	bool isValidInputEnt() const;
	bool isValidInputProp() const;

public:
	ShopRunner(ShopEntity* rootEntity);

	//HLAVNA METODA, KTORA VYKONAVA PRIKAZ
	bool run();
};
