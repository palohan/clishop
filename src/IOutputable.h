#pragma once

#include <iostream>

#define attrToStr(attrName) #attrName << ": " << _##attrName << std::endl


// hlavickovy subor; obsahuje len deklaracie metod - setri sa tym cas pri kompilacii

// rozhranie = trieda s aspon jednou cisto virtualnou metodou - tak isto zvana abstraktna trieda. neda sa vytvorit jej objekt; da s az nej len dedit; odvodene triedy musia definovat cisto virtualnu metodu print
class IOutputable
{
public:
    // abstraktna metoda; odvodene triedy ho musia definovat
	virtual std::ostream& print(std::ostream& out) const = 0;									//abstract method

	// pretazeny operator <<. a vdaka  nemu mozu odvodene triedy sa vypisat do cout
	inline friend std::ostream& operator<< (std::ostream& out, const IOutputable& _this)	
	{
		return _this.print(out);
	}

    // virtualny destruktor
	virtual ~IOutputable() {}
};