//aby sa subor pri preklade neinkludoval viackrat
#pragma once

#include <unordered_map>
#include <map>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <sstream>

#include "my-variant.h"
#include "TextKey.h"

//pred vector a map netreba pisat std::
using std::vector;
using std::unordered_map;
using std::map;
using std::string;



typedef const vector<CTextKey> StringVector;


//namespace - akoby "priezvisko" identifikatorov
namespace Meta
{
	// enum class - enum so striktnejsimi pravidlami. obsahuje symboly
	enum class Command {
		add, edit, del, set, reset, list, print, language, help, quit, last
	};

	enum class Entity {
		shop, order, item, part, last
	};

	enum class Prop {
		price, year, type, name, description, last
	};

	enum class Language
	{
		english, slovak, last
	};
}

//sablonova trieda
template<typename _Enum>
class SymbolInfo
{
	public:
	typedef _Enum Enum;
	typedef const map<Enum, CTextKey> TextKeyMap;

	protected:
	Enum _symbol;
	size_t _index = 0;
	// const char * ukazuje zvycajne na stringove konstanty, ktore sa nedaju menit . kazdy smernik ma 4 alebo 8 bytov v zavislosti na architekture cpu
	const char* _name = nullptr;

	public:
	//static StringVector symbolNames;
	static TextKeyMap symbolNames;

	/*SymbolInfo() : SymbolInfo(Enum::last)
	{}*/

	SymbolInfo(size_t index) : SymbolInfo(getSymbol(index))
	{
	}

	SymbolInfo(Enum symbol)
	{
		set(symbol);
	}

	SymbolInfo& operator=(Enum symbol)
	{
		set(symbol);
		return *this;
	}

	protected:
	void set(Enum symbol)
	{
		_symbol = symbol;
		_index = static_cast<size_t>(symbol);
				
		if (symbol < Enum::last)
			_name = getSymbolName(symbol);
	}


	public:
	bool isValid() const
	{
		return _symbol < Enum::last;
	}

	Enum getSymbol() const
	{
		return _symbol;
	}

	size_t getIndex() const
	{
		return _index;
	}

	const char* getName() const
	{
		return _name;
	}

	bool operator==(Enum symbol) const
	{
		return _symbol == symbol;
	}

	static Enum getSymbol(size_t index)
	{
		if (static_cast<size_t>(Enum::last) < index)
			throw std::out_of_range("Number is out of enum range");

		return static_cast<Enum>(index);
	}

	Enum getNextSymbol() const
	{
		return getSymbol(_index + 1);
	}

	static CTextKey getSymbolName(size_t index)
	{
		return getSymbolName(getSymbol(index));
	}

	static CTextKey getSymbolName(Enum symbol)
	{
		return symbolNames.at(symbol);
	}

	static Enum findKeyword(const string& keyword)
	{
		// auto - kompilator si sam zisti typ. je to vhodne pri dlhych typoch, ako su iteratory
		// find_if berie tri parametre- zaciatorny, koncovy iterator a lambda funkciu, ktora sa vykona pre kazdy prvok medzi nimi. tato zisti, ci sa prvok pola zacina na keyword
		auto itLong = find_if(symbolNames.begin(), symbolNames.end(), [&keyword](auto& pair) 
		{ 
			return pair.second.startsWith(keyword);
		});
			
		//nenasiel v ziadnej forme.
		if (itLong == symbolNames.end())
			return Enum::last;

		return itLong->first;
	}

	//returns the symbol if exactly ONE found; otherwise returns last
	static Enum findKeywords(const string& keyword, std::vector<CTextKey>& vecAmbigous)
	{
		vecAmbigous.clear();
		Enum symbol;

		for (const auto& [theSymbol, theName] : symbolNames)
			if (theName.startsWith(keyword))
			{
				symbol = theSymbol;
				vecAmbigous.push_back(theName);
			}
		
		if (vecAmbigous.size() == 1)
			return symbol;

		return Enum::last;						//return last if 
	}
};


template<typename Enum>
class SymbolInfo_Prop : public SymbolInfo<Enum>
{
	public:
	typedef const map<Enum, size_t> PropMap;
	static PropMap propMap;

	protected:
	size_t _props = 0;

	
	SymbolInfo_Prop(size_t index) : SymbolInfo<Enum>(index)
	{
		_props = getProps(this->_symbol);
	}

	SymbolInfo_Prop(Enum symbol) : SymbolInfo<Enum>(symbol)
	{
		_props = getProps(this->_symbol);
	}

	static size_t getProps(Enum symbol) 
	{
		auto itProp = propMap.find(symbol);
		return itProp != propMap.end() ? itProp->second : 0;
	}    
	
	public:
	//size_t je alias unsigned int (iba kladne hodnoty)
	size_t getProps() const
	{
		return _props;
	}

	bool hasProp(size_t prop) const
	{
		return (_props & prop) > 0;
	}
};


class CommandInfo : public SymbolInfo_Prop<Meta::Command>
{
    //predvolene je vsetko private
	public:
	enum Prop
	{
		doesChildren = 0x1,
		doesProps = 0x2,


		doesPlural = 0x8,
		doesList = 0x10,
		doesID = 0x20,
		doesPropVal = 0x40
	};

    //perametricky konstruktor
	CommandInfo(size_t index) : SymbolInfo_Prop(index)
	{
	}

	CommandInfo(Enum symbol) : SymbolInfo_Prop(symbol)
	{
	}

	std::stringstream usage() const;
};


class EntityInfo : public SymbolInfo_Prop<Meta::Entity>
{
	public:
	enum Prop
	{
		doesChildren = 0x1,
		doesProps = 0x2,

		isSealed = 0x4
	};

	EntityInfo(size_t index) : SymbolInfo_Prop(index)
	{
	}

	EntityInfo(Enum symbol) : SymbolInfo_Prop(symbol)
	{
	}
};


class PropertyInfo : public SymbolInfo<Meta::Prop>
{
	misc::Type _type = misc::Type::last;

	public:
	typedef const map<Enum, misc::Type> PropTypeMap;
	
	PropertyInfo() = default;

	PropertyInfo(size_t index) : SymbolInfo(index)
	{
		setExt();
	}

	PropertyInfo(Enum symbol) : SymbolInfo(symbol)
	{
		setExt();
	}

	misc::Type getType() const
	{
		return _type;
	}

	void setExt()
	{
		if (_symbol < Enum::last)
			_type = propTypes.at(_symbol);
	}

	static PropTypeMap propTypes;
};

typedef SymbolInfo<Meta::Language> LangInfo;


