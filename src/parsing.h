#pragma once


#define printNamedVal(name, val) #name << ": " << val << std::endl
#define enumVal(attr) Meta::attr##Names[static_cast<unsigned int>(m_##attr)]
#define enumToStr(attr) #attr << ": " << enumVal(attr) << std::endl

#include "IOutputable.h"
#include "misc.h"
#include "SymbolInfo.h"
#include "TextKey.h"

#include <map>
#include <vector>

using std::vector;
using std::map;




// predcasne deklaracie tried; su definovane neskor, ale kompilator o nich musi vediet uz teraz
class Parser;
class ShopRunner;


class ParserState : public IOutputable
{
public:
	// 4 verejne gettre . gettre byvaju const
	Meta::Command getCommand() const;
	Meta::Entity getEntity() const;
	size_t getId() const;

	void reset();

	// triedy Parser a ShopRunner su spriatelene triedy - vidia vsetky atributy a metody tejto triedy
	friend class Parser;
	friend class ShopRunner;

	// kedze trieda dedi z IOutputable , deklarujeme tuto metodu
	std::ostream& print(std::ostream& out) const override;


	//tieto atributy su dolezite pre chod triedy, preto su "skryte" pred vonkajsim svetom
private:
	CommandInfo _commandInfo = Meta::Command::last;
	EntityInfo _entityInfo = Meta::Entity::last;
	PropertyInfo _propertyInfo = Meta::Prop::last;
	LangInfo _langInfo = Meta::Language::english;
	size_t _id = 0;


	misc::MyVariant _variant;
	
	//will operate on all IDs
	bool _allIds = false;
	bool _isValid;

	std::vector<CTextKey> _vecAbmigous;


	template <class SymbolInfo>
	bool findKeywordsICase(SymbolInfo& symbolInfo, string& aKeyword)
	{
		symbolInfo = SymbolInfo::findKeywords(aKeyword, _vecAbmigous);

		return symbolInfo.isValid();
	}
};


#define _ambig(name) ambig##name = bad##name + ambigResult

// dolezita trieda - overuje, ci zadany vstup od uzivatela dava zmysel
class Parser
{
public:
	enum { ambigResult = 1 };

	// enum vysledkov validacie
	enum class Result {
		badCommand,
		_ambig(Command),

		badEntity,
		_ambig(Entity),
		wrongEntity,
		noEntity,

		badProp,
		_ambig(Prop),
		noProp,
		badPropVal, noPropVal,

		badID, badIDorAll, noID, noIDorAll,

		badExtra,

		badLanguage,
		_ambig(Language),
		noLanguage,

		badPlural,

		badInput, good, none
	};

	static TextKey errorMsg(Result result);


	// metoda ziskania a validacie vstupu
	Result getInput(ParserState& comObject);
	Result validateImpl(ParserState& comObject);


private:
	inline static Result makeAmbig(Result aResult)
	{
		return static_cast<Result>(static_cast<int>(aResult) + ambigResult);
	}

	// textova reprezentacia vysledkov validacie
	typedef const map<Result, TextKey> ParseResultTextKeys;
	static ParseResultTextKeys _errorStrs;


	std::istringstream iss;
	std::string inputLine;

	std::streamoff _prevPos;
};
