using namespace std;

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <array>
#include <stdexcept>


#include "misc.h"
#include "parsing.h"
#include "IOutputable.h"
#include "Entity.h"


EntityBase::EntityBase(Meta::Entity entity, EntityBase* parent/* = nullptr*/) :
	_entityInfo(entity),
    _parent(parent)
{
    for (const EntityBase* entIt = getParent(); entIt && entIt->_entityInfo.getIndex() > 0; entIt = entIt->getParent())
        _allPrompt = entIt->_prompt + " - " + _allPrompt;

    _allPrompt += _prompt = _entityInfo.getName();
}


EntityBase* EntityBase::getParent() const
{
    return _parent;
}

//destruktor
EntityBase::~EntityBase()
{
}


Entity::Entity(Meta::Entity entity, size_t id/* = 1*/, Entity* parent/* = nullptr*/) :
    EntityBase(entity, parent),
    _id(id),
    _lastChildId(0)
{
    if (_entityInfo.getIndex() > 0)
    {
        _allPrompt += " " + to_string(_id);
        _prompt += " " + to_string(_id);
    }
}


bool Entity::canHaveChildren() const
{
	return _entityInfo.hasProp(EntityInfo::doesChildren);
}


bool Entity::canHaveChildOf(Meta::Entity otherEntity) const
{
    bool result = canHaveChildren() && _entityInfo.getNextSymbol() == otherEntity;

    if (!result)
        cout << "Error: Cannot access " << EntityInfo::getSymbolName(otherEntity) << "s from this level" << endl;

    return result;
}


bool Entity::isParentOf(Meta::Entity otherEntity, size_t id) const
{
    if (canHaveChildOf(otherEntity))
    {
        if (!hasChild(id))
            cout << "Error: "<< EntityInfo::getSymbolName(otherEntity) << " " << id << " does not exist" << endl;
        else
            return true;
    }

    return false;
}


bool Entity::hasChild(size_t id) const
{
    auto it = _childEntities.find(id);

    return it != _childEntities.end();
}


Entity* Entity::add()
{
    if (canHaveChildren())
    {
        ++_lastChildId;

        auto [the_id_entity, isNew] = _childEntities.emplace(_lastChildId, addInternal(_lastChildId));

        cout << "Added " << the_id_entity->second->_entityInfo.getName() << " " << the_id_entity->second->_id << endl;

        return the_id_entity->second.get();
    }
    else
        return nullptr;															//this should never happen !!
}


std::unique_ptr<Entity> Entity::addInternal(size_t newId)
{
    return Entity::addInternal(newId, *this);
}

/*
Entity* Entity::edit(size_t id)
{
    return getChild(id);
}*/


Entity* Entity::getChild(size_t id) const
{
    return getChildIter(id)->second.get();
}

auto Entity::getChildIter(size_t id) const -> decltype(_childEntities)::const_iterator
{
	auto it = _childEntities.find(id);

	if (it == _childEntities.end())
		throw out_of_range("getChildIter: iterator out of range");

	return it;
}


void Entity::del(size_t id)
{
    auto it = getChildIter(id);

    cout << "Deleted " << it->second->_prompt << endl;

    _childEntities.erase(it);
}


void Entity::delAll()
{
    if (_childEntities.empty())
    {
        cout << "No " << EntityInfo::getSymbolName(_entityInfo.getNextSymbol()) << "s exist" << endl;
        return;
    }

    for (const auto& child : _childEntities)
        cout << "Deleted " << child.second->_prompt << endl;

    _childEntities.clear();
}

std::ostream& Entity::print(std::ostream& out) const
{
    listImpl(out);

    return out;
}

void Entity::listImpl(std::ostream& out, size_t tabLevel/* = 0*/) const
{
    string tabs(tabLevel, '\t');

    if (tabLevel == 0)
        out << endl << "----------------------------------------" << endl;

    out << tabs << _prompt << endl;

    tabs += '\t';

    listInternal(out, tabs);

	Meta::Entity childEntity = _entityInfo.getNextSymbol();

    if (childEntity < Meta::Entity::last)
        out << tabs << "total " << EntityInfo::getSymbolName(childEntity) << "s: " << _childEntities.size() << endl;

    out << endl;

    for (const auto& child : _childEntities)
        child.second->listImpl(out, tabLevel + 1);

    if (tabLevel == 0)
        out << "----------------------------------------" << endl << endl;
}


//vypis do suboru
bool Entity::print() const
{
    string fileName = _allPrompt + ".txt";

    ofstream file(fileName);

    if (!file)
    {
        cerr << "Error: Cannot create file'" << fileName << "'" << endl;
        return false;
    }

	file << *this;
	cout << "Saved to '" << fileName << "'" << endl;

    return true;
}


void Entity::listInternal(std::ostream& out, const string& tabs) const					//placeholder for derived classes
{
}


void Entity::printPrompt() const
{
    cout << _allPrompt << "> " << flush;
}

//
//	Entity* getParent() const
//	{
//		return _parent;
//	}

//placeholder
double Entity::getTotalPrice() const
{
    return 0;
}


ShopEntity::ShopEntity(Meta::Entity entity, size_t id/* = 1*/, Entity* parent/* = nullptr*/) : Entity(entity, id, parent)
{
    if (!_entityInfo.hasProp(EntityInfo::isSealed))
		for (const auto& [theProp, theType] : PropertyInfo::propTypes)
			_propMap.emplace(theProp, theType);
}


bool ShopEntity::setProp(Meta::Prop symbol, const misc::MyVariant& vrt)
{
    auto& prop = _propMap.at(symbol);												//just let it throw - all should be validated here

    if (!prop.copyOther(vrt))
    {
        cerr << "Error: trying to set a wrong property type" << endl;			//this should NEVER happen at this point
        return false;
    }

	cout << PropertyInfo::getSymbolName(symbol) << " set to " << vrt << endl;

    return true;
}


void ShopEntity::resetProp(Meta::Prop symbol)
{
	auto& prop = _propMap.at(symbol);												//just let it throw - all should be validated here

    prop.reset();

	cout << PropertyInfo::getSymbolName(symbol) << " reset" << endl;
}


double ShopEntity::getTotalPrice() const
{
    auto thePrice = propVal(price).getDouble();								//base price
    double totalPrice = thePrice ? *thePrice : 0;

    for (const auto& child : _childEntities)
        totalPrice += child.second->getTotalPrice();			//rekurzivna funkcia

    return totalPrice;
}


std::unique_ptr<Entity> ShopEntity::addInternal(size_t newId)
{
    return Entity::addInternal(newId, *this);
}


//placeholder for derived classes
void ShopEntity::listInternal(std::ostream& out, const string& tabs) const
{
    //entity od shopu nahor toto vypisu
    if (_entityInfo.hasProp(EntityInfo::doesProps))
    {
        out << tabs << propToStr(price);
        out << tabs << propToStr(year);
        out << tabs << propToStr(type);
        out << tabs << propToStr(name);
        out << tabs << propToStr(description);


		if (_entityInfo.hasProp(EntityInfo::doesChildren))
        {
            auto totalPrice = getTotalPrice();

            out << endl << tabs << "total price: " << getTotalPrice() << endl;

            string pricePerItem = _childEntities.size() ? to_string(totalPrice / _childEntities.size()) : "-";

            out << tabs << "price per " << EntityInfo::getSymbolName(_entityInfo.getNextSymbol()) << ": " << pricePerItem << endl;
        }
    }
}


ostream& ShopEntity::printHelp(ostream& out) const
{
	size_t eProps = _entityInfo.getProps();

	out << "Available commands:" << endl;

	for (const auto& [theCmd, theProps] : CommandInfo::propMap)
		if (theProps == 0 || eProps & theProps)
			printHelpOne(theCmd, out);

	return out;
}


ostream& ShopEntity::printHelpOne(const CommandInfo& cInfo, ostream& out) const
{
	auto cName = cInfo.getName();

	char toPrepend = '\t';

	static size_t lastPropType = 0;


	if (cInfo.hasProp(CommandInfo::doesChildren))
	{
		lastPropType = CommandInfo::doesChildren;

		auto childName = EntityInfo::getSymbolName(_entityInfo.getNextSymbol());

		if ((cInfo.getProps() ^ CommandInfo::doesChildren) == 0)				//no IDs or plural
			out << toPrepend << cName << " " << childName << endl;
		else
		{
			if (cInfo.hasProp(CommandInfo::doesID))
				for (const auto& child : _childEntities)
					out << toPrepend << cName << " " << childName << " " << child.first << endl;

			if (cInfo.hasProp(CommandInfo::doesPlural))
				out << toPrepend << cName << " " << childName << " " << getText(TextKey::all) << endl;
		}
	}
	else if (cInfo.hasProp(CommandInfo::doesProps))
	{
		if ((lastPropType & CommandInfo::doesProps) != CommandInfo::doesProps)
			cout << endl;

		lastPropType = CommandInfo::doesProps;

		for (const auto& [theSymbol, theType] : PropertyInfo::propTypes)
		{
			out << toPrepend << cName << " " << PropertyInfo::getSymbolName(theSymbol);

			if (cInfo.hasProp(CommandInfo::doesPropVal))
			{
				lastPropType &= CommandInfo::doesPropVal;

				out << getTypeHint(theType);
			}

			out << endl;
		}
	}
	else
	{
		if (lastPropType)
			cout << endl;

		lastPropType = 0;

		out << toPrepend << cName << endl;
	}

	return out;
}
