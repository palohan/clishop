#include "my-variant.h"

#include <stdexcept>


using namespace std;
using namespace misc;

MyVariant::MyVariant() : _type(Type::last), _isSealed(false)
{}

MyVariant::MyVariant(Type type) : _type(type), _isSealed(true)
{
	if (type >= Type::last)
		throw logic_error("cant set variant to incorrect type");
}

MyVariant::MyVariant(int num) : _isSealed(false)
{
	MyVariant::setInt(num);
}

MyVariant::MyVariant(double num) : _isSealed(false)
{
	MyVariant::setDouble(num);
}

MyVariant::MyVariant(const std::string& str) : _isSealed(false)
{
	MyVariant::setString(str);
}

MyVariant::MyVariant(const MyVariant& other) :
	_type(other._type),
	_isSealed(other._isSealed)
{
	if (!copyOther(other))
		throw logic_error("variant copy ctor failed");

	_isClear = other._isClear;
}

std::optional<size_t> MyVariant::getInt() const
{
	if (_type == Type::uint)
	{
		return _int.getValue();
	}

	return {};
}

std::optional<double> MyVariant::getDouble() const
{
	if (_type == Type::floating)
	{
		return _double.getValue();
	}

	return {};
}

std::optional<string> MyVariant::getString() const
{
	if (_type == Type::string)
	{
		return _string;
	}

	return {};
}

bool MyVariant::setInt(int num)
{
	if (!canOperate(Type::uint))
		return false;

	_type = Type::uint;

	if (_int.setValue(num))
	{
		_isClear = false;
		return true;
	}

	return false;
}

bool MyVariant::setDouble(double num)
{
	if (!canOperate(Type::floating))
		return false;

	_type = Type::floating;

	if (_double.setValue(num))
	{
		_isClear = false;
		return true;
	}

	return false;
}

bool MyVariant::setString(const std::string& str)
{
	if (!canOperate(Type::string))
		return false;

	_type = Type::string;
	_string = str;

	_isClear = false;

	return true;
}

bool MyVariant::setGeneric(istringstream& iss, Type targetType)
{
	if (!canOperate(targetType))
		return false;

	if (iss.eof())														//nothing to read
		return false;

	double dbl = 0;
	int num = 0;
	string str;

	switch (targetType)
	{
	case Type::uint:
		iss >> skipws >> num;
		break;

	case Type::floating:
		iss >> skipws >> dbl;
		break;

	case Type::string:												//special handling: get remaining string
		ws(iss);													//consume leading spaces
		getline(iss, str);											//nechceme, aby citanie zastalo na medzere, tak pouzijeme getline
																	//iss >> skipws >> str;
		break;

	case Type::last:
		return false;
	}

	if (iss.fail())
		return false;


	switch (targetType)
	{
	case Type::uint:
		return setInt(num);

	case Type::floating:
		return setDouble(dbl);

	case Type::string:
		return setString(str);

	case Type::last:
		return false;
	}

	return false;
}


bool MyVariant::copyOther(const MyVariant& other)
{
	Type otherType = other.getType();

	if (!canOperate(otherType))
		return false;

	switch (otherType)
	{
	case Type::uint:
		if (auto theVal = other.getInt())
			return setInt(*theVal);
		break;

	case Type::floating:
		if (auto theVal = other.getDouble())
			return setDouble(*theVal);
		break;

	case Type::string:
		if (auto theVal = other.getString())
			return setString(*theVal);
		break;

	case Type::last:
		return false;
	}

	return false;
}


void MyVariant::reset()
{
	_string.clear();
	_int = 0;
	_double = 0;

	_isClear = true;
}


bool MyVariant::canOperate(Type typ) const
{
	return !_isSealed || typ == _type;									//either _isSealed is false, or types must match
}

std::ostream& MyVariant::print(ostream& out) const
{
	if (!_isClear)
		switch (_type)
		{
		case Type::uint:
			return out << _int;

		case Type::floating:
			return out << _double;

		case Type::string:
			return out << _string;

		case Type::last:
			break;
		}

	return out;
}

const char* misc::getTypeHint(misc::Type type)
{
	switch (type)
	{
	case Type::floating:
		return " <12.34>";

	case Type::uint:
		return " <1234>";

	case Type::string:
		return " <abcd>";

	default:
		return "";
	}
}