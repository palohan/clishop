#include "misc.h"
#include <cstring>

#ifdef _MSC_VER 
#define strncasecmp _strnicmp
#endif

// funkcia vrati true, ak sa druhy string nachadza v prvom
bool misc::startsWith(const char* bigString, const char* smallString, unsigned int count/* = 0*/)
{
	return strncasecmp(bigString, smallString, count ? count : strlen(smallString)) == 0;
}