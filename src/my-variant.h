#pragma once

#include <sstream>
#include <string>
#include <optional>
#include "IOutputable.h"


// trieda symulujuca kladne cislo (cele, desatinne). dedi od rozhrania  IOutputable.
//  dedicnost je verejna - tato trieda ma verejny pristup k veciam rozhrania  IOutputable
// sablonova trieda. parameter sablony numeric musi byt ciselny typ
template<typename numeric>
class positiveNumber : public IOutputable
{
private:
	numeric _value;

	std::ostream& print(std::ostream& out) const
	{
		return out << _value;
	}

public:
	// konstruktor parametricky - s defaultnym parametrom
	positiveNumber(numeric value = 0)
	{
		setValue(value);
	}
	// metoda SETTER. berie int; ak je vacsi alebo rovny nule nastavi nan hodnotu svojho atributu _value
	bool setValue(numeric value)
	{
		if (value >= 0)
		{
			_value = value;
			return true;
		}

		return false;
	}

	// setter; snazi sa precitat zo streamu cislo. ak to vyjde, nastavi sa na nu.
	bool setValue(std::istringstream& iss)
	{
		numeric value;
		iss >> std::skipws >> value;

		if (iss.fail())
			return false;

		return setValue(value);
	}

	// metoda getter; vracia atribut _value. je const, lebo nemeni stav objektu
	numeric getValue() const
	{
		return _value;
	}
};


namespace misc
{
	// enumeracia typov
	enum class Type { uint, floating, string, last };
	const char* getTypeHint(Type type);

	class MyVariant : public IOutputable
	{
		std::string _string;
		positiveNumber<int> _int;
		positiveNumber<double> _double;

		Type _type;
		const bool _isSealed;

		bool _isClear = true;

		std::ostream& print(std::ostream& out) const;

		bool canOperate(Type typ) const;

		MyVariant& operator=(const MyVariant& other) = delete;

	public:
		MyVariant(const MyVariant& other);

		MyVariant(int num);
		MyVariant(double num);
		MyVariant(const std::string& str);
		MyVariant(Type type);
		MyVariant();

		inline Type getType() const { return _type; }

		std::optional<size_t> getInt() const;
		std::optional<double> getDouble() const;
		std::optional<std::string> getString() const;

		bool setInt(int num);
		bool setDouble(double num);
		bool setString(const std::string& str);

		bool setGeneric(std::istringstream& iss, Type targetType);

		bool copyOther(const MyVariant& other);

		void reset();
	};
}
