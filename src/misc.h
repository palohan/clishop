#pragma once

namespace misc
{
	bool startsWith(const char* bigString, const char* smallString, unsigned int count = 0);
}