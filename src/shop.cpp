#include "shop.h"
#include <iostream>

using namespace std;

ShopRunner::ShopRunner(ShopEntity* rootEntity) : _curEntity(rootEntity)
{}

bool ShopRunner::isValidInput() const
{
	auto cmd = _parserState.getCommand();

	if (cmd >= Meta::Command::list)									//these dont need anything else
		return true;

	if (_parserState.getEntity() < Meta::Entity::last)				//we're dealing with entities
		return isValidInputEnt();
	else																	//we're dealing with props
		return isValidInputProp();
}

bool ShopRunner::isValidInputEnt() const
{
	if (!_curEntity->canHaveChildOf(_parserState.getEntity()))
		return false;


	auto id = _parserState.getId();

	if (_parserState._commandInfo.hasProp(CommandInfo::doesID))
		if (!_parserState._commandInfo.hasProp(CommandInfo::doesPlural) || !_parserState._allIds)
			if (!_curEntity->hasChild(id))
			{
				std::cerr << "Error: No such " << _parserState._entityInfo.getName() << " exists" << std::endl;
				return false;
			}

	return true;
}

bool ShopRunner::isValidInputProp() const
{
	if (!_curEntity->_entityInfo.hasProp(EntityInfo::doesProps))							//cannot set on shop
	{
		std::cerr << "Error: Cannot alter properties on " << _curEntity->_entityInfo.getName() << std::endl;
		return false;
	}

	if (_parserState._commandInfo.hasProp(CommandInfo::doesProps))
		if (_parserState._propertyInfo.isValid())
			return true;

	return false;
}

bool ShopRunner::run()
{
	_curEntity->printPrompt();

	if (_parser.getInput(_parserState) == Parser::Result::good)
	{
		if (isValidInput())													//all is validated here
		{
			auto id = _parserState.getId();

			//kazda vetva ovlada 1 prikaz
			switch (_parserState.getCommand())								//lets process the commands
			{
			case Meta::Command::quit:
				if (_curEntity->_entityInfo.getIndex() == 0)							//quitting at shop level
				{
					std::cout << "Exiting" << std::endl;
					return false;										//SKONCI PROGRAM
				}
				else
				{
					_curEntity = dynamic_cast<ShopEntity*>(_curEntity->getParent());

					if (_curEntity == nullptr)								//this should never happen
						throw std::logic_error("Must have parent");
				}
				break;

			case Meta::Command::list:
				std::cout << *_curEntity;
				return true;

			case Meta::Command::print:
				_curEntity->print();
				return true;

			case Meta::Command::add:
			{
				auto newChild = _curEntity->add();

				if (newChild)
					_curEntity = dynamic_cast<ShopEntity*>(newChild);
				else
					throw std::logic_error("Command::add");
			}

			break;

			case Meta::Command::edit:
				_curEntity = dynamic_cast<ShopEntity*>(_curEntity->getChild(id));
				break;

			case Meta::Command::del:
				if (_parserState._allIds)
					_curEntity->delAll();
				else
					_curEntity->del(id);
				break;

			case Meta::Command::set:
				if (!_curEntity->setProp(_parserState._propertyInfo.getSymbol(), _parserState._variant))
				{
					//TODO: handle error
				}
				break;

			case Meta::Command::reset:
				_curEntity->resetProp(_parserState._propertyInfo.getSymbol());
				break;

			case Meta::Command::help:
				_curEntity->printHelp(cout);
				return true;

			case Meta::Command::language:
				if (_parserState._langInfo == Meta::Language::slovak)
					TextKeyMap_CUR = &TextKeyMap_SK;
				else
					TextKeyMap_CUR = &TextKeyMap_EN;

				clog << _parserState._langInfo.getName() << endl;
				return true;

			case Meta::Command::last:
				return false;
			}
		}
	}

	return true;
}